U#!/bin/bash

phone_file="phone_directory.txt"

# Function to add a new entry
add_entry() {
    echo "Enter name:"
    read name
    echo "Enter phone number:"
    read phone_number
    echo "$name,$phone_number" >> "$phone_file"
    echo "Entry added successfully."
}

# Function to view all entries
view_entries() {
    echo "Phone Directory:"
    cat "$phone_file"
}

# Function to update an entry
update_entry() {
    echo "Enter name to update:"
    read name
    echo "Enter new phone number:"
    read new_phone_number
    sed -i "/^$name,/ s/,.*/,$new_phone_number/" "$phone_file"
    echo "Entry updated successfully."
}

# Function to delete an entry
delete_entry() {
    echo "Enter name to delete:"
    read name
    sed -i "/^$name,/d" "$phone_file"
    echo "Entry deleted successfully."
}

# Main menu
while true; do
    echo "Phone Directory Menu:"
    echo "1. Add Entry"
    echo "2. View Entries"
    echo "3. Update Entry"
    echo "4. Delete Entry"
    echo "5. Exit"
    read -p "Enter your choice: " choice
    case $choice in
        1) add_entry ;;
        2) view_entries ;;
        3) update_entry ;;
        4) delete_entry ;;
        5) echo "Exiting..."; exit ;;
        *) echo "Invalid choice. Please enter a number between 1 and 5." ;;
    esac
done

